package main

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gomessanger/service"
	"gomessanger/usecases"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"

	messagerserver "gomessanger/proto"
	chatrepo "gomessanger/repositories/chat/repository"
	messagerepo "gomessanger/repositories/message/repository"
	userrepo "gomessanger/repositories/user/repository"
)

func main() {
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}

	pool := DB()
	userRepository := userrepo.NewPostgres(pool)
	chatRepository := chatrepo.NewPostgres(pool)
	massageRepository := messagerepo.NewPostgres(pool)

	userUseCases := usecases.User{UserRepository: userRepository}
	chatUseCases := usecases.Chat{ChatRepo: chatRepository, MessagesRepo: massageRepository}
	messageUseCases := usecases.Message{MessageRepo: massageRepository, ChatRepo: chatRepository}

	userService := service.User{UserUseCases: userUseCases}
	chatService := service.Chat{ChatUseCases: chatUseCases}
	messageService := service.Message{MessageUseCases: messageUseCases}

	server := grpc.NewServer()
	messagerserver.RegisterUserServer(server, &service.GRPCService{User: userService})
	messagerserver.RegisterMessageServer(server, &service.GRPCService{Message: messageService})
	messagerserver.RegisterChatServer(server, &service.GRPCService{Chat: chatService})

	reflection.Register(server)
	if err = server.Serve(listener); err != nil {
		log.Fatal("can not connect to server: ", err)
	}

}

func DB() *pgxpool.Pool {
	con, err := pgxpool.Connect(context.Background(), "postgres://postgres:postgres@192.168.0.5:5432/postgres?sslmode=disable&pool_max_conns=10&pool_min_conns=0&pool_max_conn_lifetime=1h&pool_max_conn_idle_time=30m") //TODO
	if err != nil {
		log.Fatal("cannot connect to db", err)
	}

	if err = con.Ping(context.Background()); err != nil {
		log.Fatal(err)
	}

	return con
}
