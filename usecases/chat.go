package usecases

import (
	"context"
	"errors"
	"fmt"
	"gomessanger/jwttoken"
	"gomessanger/repositories/chat"
	"gomessanger/repositories/message"
)

var (
	ErrChatIsExist = errors.New("chat is exist")
	ErrUserInChat  = errors.New("user already in chat")
)

type Chat struct {
	ChatRepo     chat.Repository
	MessagesRepo message.Repository
}

func (ch *Chat) CreateChat(ctx context.Context, token string, chatID, userID uint64) error {
	valid, err := jwttoken.IsValid(token)
	if !valid {
		return ErrTokenNotValid
	}

	exist, err := ch.ChatRepo.IsUserInChat(ctx, chatID, userID)
	if exist {
		return ErrChatIsExist
	}
	if err != nil {
		return fmt.Errorf("cannot create chat | %w", err)
	}

	if err = ch.ChatRepo.SetUserInChat(ctx, chatID, userID); err != nil {
		return fmt.Errorf("cannot create chat | %w", err)
	}

	return nil
}

func (ch *Chat) DeleteChat(ctx context.Context, token string, chatID uint64) error {
	valid, err := jwttoken.IsValid(token)
	if !valid {
		return ErrTokenNotValid
	}

	if err = ch.ChatRepo.DeleteChat(ctx, chatID); err != nil {
		return fmt.Errorf("cannot delete chat | %w", err)
	}

	if err = ch.MessagesRepo.DeleteMessages(ctx, chatID); err != nil {
		return fmt.Errorf("cannot delete chat messages | %w", err)
	}

	return nil
}

func (ch *Chat) JoinChat(ctx context.Context, token string, chatID, userID uint64) error {
	valid, err := jwttoken.IsValid(token)
	if !valid {
		return ErrTokenNotValid
	}

	isUserInChat, err := ch.ChatRepo.IsUserInChat(ctx, chatID, userID)
	if isUserInChat {
		return ErrUserInChat
	}
	if err != nil {
		return fmt.Errorf("cannot join chat | %w", err)
	}

	if err = ch.ChatRepo.SetUserInChat(ctx, chatID, userID); err != nil {
		return fmt.Errorf("cannot join chat | %w", err)
	}
	return nil
}

func (ch *Chat) LeaveChat(ctx context.Context, token string, chatID, userID uint64) error {
	valid, err := jwttoken.IsValid(token)
	if !valid {
		return ErrTokenNotValid
	}

	if err = ch.ChatRepo.DropUserFromChat(ctx, chatID, userID); err != nil {
		return fmt.Errorf("cannot leave chat | %w", err)
	}

	return nil
}
