package usecases

import (
	"context"
	"errors"
	"fmt"
	"gomessanger/jwttoken"
	"gomessanger/repositories/user"
)

var (
	ErrUserAlreadyExists = errors.New("user already exists")
	ErrLoginOrPassWrong  = errors.New("login or password is wrong")
)

type User struct {
	UserRepository user.Repository
}

func (u *User) Register(ctx context.Context, login, pass string) error {
	exist, err := u.UserRepository.Exist(ctx, login)
	if err != nil {
		return fmt.Errorf("register failed | %w", err)
	}
	if exist {
		return ErrUserAlreadyExists
	}

	if err = u.UserRepository.Insert(ctx, login, pass); err != nil {
		return fmt.Errorf("register failed | %w", err)
	}

	return nil
}

func (u *User) Login(ctx context.Context, login, pass string) (string, error) {
	loginSuccess, err := u.UserRepository.Login(ctx, login, pass)
	if err != nil {
		return "", fmt.Errorf("login failed | %w", err)
	}

	if !loginSuccess {
		return "", fmt.Errorf("login failed | %w", ErrLoginOrPassWrong)
	}

	return jwttoken.Token(login)
}
