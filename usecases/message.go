package usecases

import (
	"context"
	"errors"
	"fmt"
	"gomessanger/repositories/chat"
	"time"

	"gomessanger/jwttoken"
	"gomessanger/repositories/message"
	"gomessanger/repositories/message/repository"
)

type Message struct {
	MessageRepo message.Repository
	ChatRepo    chat.Repository
}

type unreadMessage struct {
	ChatID, UserID uint64
	MessageText    string
	DateSent       time.Time
}

var (
	ErrTokenNotValid = errors.New("token is not valid")
	ErrNoNewMessages = errors.New("no new messages")
)

func (m *Message) SendMessage(
	ctx context.Context, token string, chatID, userID uint64, messageText, dateSent string,
) error {
	valid, err := jwttoken.IsValid(token)
	if !valid {
		return ErrTokenNotValid
	}

	//check for chat existence, if not - create
	userInChat, err := m.ChatRepo.IsUserInChat(ctx, chatID, userID)
	if !userInChat {
		if err = m.ChatRepo.SetUserInChat(ctx, chatID, userID); err != nil {
			return fmt.Errorf("cannot send message | %w", err)
		}
	}

	//if chat exists, set message into it
	err = m.MessageRepo.SetMessage(ctx, chatID, userID, messageText, dateSent)
	if err != nil {
		return fmt.Errorf("cannot send message | %w", err)
	}

	//set last time action in the users_chat table
	err = m.ChatRepo.SetLastChatReadingTime(ctx, chatID, userID)
	if err != nil {
		return fmt.Errorf("cannot save chat reading time | %w", err)
	}

	return nil
}

func (m *Message) GetUnreadMessages(
	ctx context.Context, token string, chatID, userID uint64,
) ([]unreadMessage, error) {
	valid, err := jwttoken.IsValid(token)
	if !valid {
		return nil, ErrTokenNotValid
	}

	messages, err := m.MessageRepo.GetUnreadMessages(ctx, chatID, userID)
	if errors.Is(err, repository.ErrNoNewMessages) {
		return nil, ErrNoNewMessages
	}
	if err != nil {
		return nil, fmt.Errorf("cannot update messages | %w", err)
	}

	var unreadMessages []unreadMessage

	for _, val := range messages {
		unreadMessages = append(unreadMessages, unreadMessage{
			ChatID:      val.ChatID,
			UserID:      val.UserID,
			MessageText: val.MessageText,
			DateSent:    val.DateSent,
		})
	}

	err = m.ChatRepo.SetLastChatReadingTime(ctx, chatID, userID)
	if err != nil {
		return nil, fmt.Errorf("cannot save chat reading time | %w", err)
	}

	return unreadMessages, nil

}
