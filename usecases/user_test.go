package usecases

import (
	"context"
	"github.com/golang/mock/gomock"
	"gomessanger/repositories/user"
	userrepo "gomessanger/repositories/user/repository"
	"testing"
)

func TestUser_Login(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	userRepo := userrepo.NewMockRepository(ctrl)
	userRepo.EXPECT().Login(gomock.Any(), "success", gomock.Any()).AnyTimes().Return(true, nil)
	userRepo.EXPECT().Login(gomock.Any(), "fail", gomock.Any()).AnyTimes().Return(false, nil)

	type fields struct {
		UserRepository user.Repository
	}
	type args struct {
		ctx   context.Context
		login string
		pass  string
	}
	tests := []struct {
		fields    fields
		args      args
		name      string
		wantErr   bool
		wantToken bool
	}{
		{
			fields:    fields{userRepo},
			args:      args{context.Background(), "success", ""},
			name:      "",
			wantErr:   false,
			wantToken: true,
		},

		{
			fields:    fields{userRepo},
			args:      args{context.Background(), "fail", ""},
			name:      "",
			wantErr:   true,
			wantToken: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				UserRepository: tt.fields.UserRepository,
			}
			got, err := u.Login(tt.args.ctx, tt.args.login, tt.args.pass)
			if (err != nil) != tt.wantErr {
				t.Errorf("Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == "" == tt.wantToken {
				t.Errorf("Login() got = %v", got)
			}
		})
	}
}

func TestUser_Register(t *testing.T) {
	type fields struct {
		UserRepository user.Repository
	}
	type args struct {
		ctx   context.Context
		login string
		pass  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				UserRepository: tt.fields.UserRepository,
			}
			if err := u.Register(tt.args.ctx, tt.args.login, tt.args.pass); (err != nil) != tt.wantErr {
				t.Errorf("Register() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
