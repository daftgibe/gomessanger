package jwttoken

import (
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

var ErrSignatureInvalid = errors.New("signature is invalid")

type Claim struct {
	Username string
	jwt.RegisteredClaims
}

var jwtKey = []byte("mklgsdfkg34rp8w9fsdfgvdfjkl")

func Token(login string) (string, error) {
	expirationTime := time.Now().Add(24 * time.Hour)

	claims := &Claim{
		Username: login,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", fmt.Errorf("cannot sign token | %w", err)
	}

	return tokenString, nil
}

func IsValid(token string) (bool, error) {
	claim := &Claim{}

	parsedToken, err := jwt.ParseWithClaims(token, claim, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return false, ErrSignatureInvalid
		}

		return false, fmt.Errorf("cannot parse token %w", err)
	}

	if !parsedToken.Valid {
		return false, nil
	}

	return true, nil
}
