package service

import (
	"context"
	"errors"
	"fmt"
	messagerserver "gomessanger/proto"
	"gomessanger/usecases"
)

var ErrNoNewMessages = errors.New("no new messages")

type Message struct {
	MessageUseCases usecases.Message
}

func (m *Message) SendMessage(
	ctx context.Context, token string, chatID, userID uint64, messageText, dateSent string,
) error {
	err := m.MessageUseCases.SendMessage(ctx, token, chatID, userID, messageText, dateSent)
	if err != nil {
		return fmt.Errorf("cannot send message | %w", err)
	}

	return nil
}

func (m *Message) GetUnreadMessages(
	ctx context.Context, token string, chatID, userID uint64,
) ([]*messagerserver.UnreadMessage, error) {
	messages, err := m.MessageUseCases.GetUnreadMessages(ctx, token, chatID, userID)
	if errors.Is(err, usecases.ErrNoNewMessages) {
		return nil, ErrNoNewMessages
	}

	if err != nil {
		return nil, fmt.Errorf("cannot update messages | %w", err)
	}

	var unreadMessages []*messagerserver.UnreadMessage

	for _, val := range messages {
		unreadMessages = append(unreadMessages, &messagerserver.UnreadMessage{
			ChatID:      val.ChatID,
			UserID:      val.UserID,
			MessageText: val.MessageText,
			DateSent:    val.DateSent.Format("2006-01-02 15:04:05"),
		})
	}

	return unreadMessages, nil

}
