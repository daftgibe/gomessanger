package service

import (
	"context"
	"errors"
	messagerserver "gomessanger/proto"
)

type GRPCService struct {
	messagerserver.UnimplementedUserServer
	User User
	messagerserver.UnimplementedMessageServer
	Message Message
	messagerserver.UnimplementedChatServer
	Chat Chat
}

// user client
func (s *GRPCService) Registration(
	ctx context.Context, req *messagerserver.RegistrationRequest,
) (*messagerserver.RegistrationResponse, error) {
	err := s.User.Register(ctx, req.Login, req.Pass)

	switch {
	case errors.Is(err, ErrUserAlreadyExists):
		return &messagerserver.RegistrationResponse{
			RegistrationResult: messagerserver.RegistrationResult_AlreadyExists,
		}, nil
	case errors.Is(err, ErrUnknown):
		return &messagerserver.RegistrationResponse{
			RegistrationResult: messagerserver.RegistrationResult_RegisterError,
		}, nil
	default:
		return &messagerserver.RegistrationResponse{
			RegistrationResult: messagerserver.RegistrationResult_Success,
		}, nil
	}
}

func (s *GRPCService) Login(ctx context.Context, req *messagerserver.LoginRequest) (*messagerserver.LoginResponse, error) {
	loginToken, err := s.User.Login(ctx, req.Login, req.Pass)

	switch {
	case errors.Is(err, ErrLoginOrPassWrong):
		return &messagerserver.LoginResponse{
			Token:       "",
			LoginResult: messagerserver.LoginStatus_InvalidCredentials,
		}, nil
	case err != nil:
		return &messagerserver.LoginResponse{
			Token:       "",
			LoginResult: messagerserver.LoginStatus_LoginError,
		}, nil

	default:
		return &messagerserver.LoginResponse{
			Token:       loginToken,
			LoginResult: messagerserver.LoginStatus_Ok,
		}, nil
	}

}

// message client
func (s *GRPCService) SendMessage(
	ctx context.Context, req *messagerserver.SendMessageRequest,
) (*messagerserver.SendMessageResponse, error) {
	err := s.Message.SendMessage(ctx, req.Token, req.ChatID, req.UserID, req.MessageText, req.DateSent)
	if err != nil {
		return &messagerserver.SendMessageResponse{SendResult: messagerserver.SendStatus_NotSent}, nil
	}

	return &messagerserver.SendMessageResponse{SendResult: messagerserver.SendStatus_Sent}, nil
}

func (s *GRPCService) GetUnreadMessages(
	ctx context.Context, req *messagerserver.GetUnreadMessagesRequest,
) (*messagerserver.GetUnreadMessagesResponse, error) {
	unreadMessages, err := s.Message.GetUnreadMessages(ctx, req.Token, req.ChatID, req.UserID)

	switch {
	case errors.Is(err, ErrNoNewMessages):
		return &messagerserver.GetUnreadMessagesResponse{
			UnreadMessages: nil,
			GetStatus:      messagerserver.GetStatus_NoNewMessages,
		}, nil
	case err != nil:
		return &messagerserver.GetUnreadMessagesResponse{
			UnreadMessages: nil,
			GetStatus:      messagerserver.GetStatus_UpdateError,
		}, nil
	default:
		return &messagerserver.GetUnreadMessagesResponse{
			UnreadMessages: unreadMessages,
			GetStatus:      messagerserver.GetStatus_NewMessagesReceived,
		}, nil
	}
}

// chat client
func (s *GRPCService) CreateChat(
	ctx context.Context, req *messagerserver.CreateChatRequest,
) (*messagerserver.CreateChatResponse, error) {
	if err := s.Chat.CreateChat(ctx, req.Token, req.ChatID, req.UserID); err != nil {
		return &messagerserver.CreateChatResponse{CreateChatStatus: messagerserver.CreateChatStatus_CreateUnsuccessful}, nil
	}

	return &messagerserver.CreateChatResponse{CreateChatStatus: messagerserver.CreateChatStatus_CreateSuccessful}, nil
}

func (s *GRPCService) DeleteChat(
	ctx context.Context, req *messagerserver.DeleteChatRequest,
) (*messagerserver.DeleteChatResponse, error) {
	if err := s.Chat.DeleteChat(ctx, req.Token, req.ChatID); err != nil {
		return &messagerserver.DeleteChatResponse{DeleteChatStatus: messagerserver.DeleteChatStatus_DeleteUnsuccessful}, nil
	}

	return &messagerserver.DeleteChatResponse{DeleteChatStatus: messagerserver.DeleteChatStatus_DeleteSuccessful}, nil
}

func (s *GRPCService) JoinChat(
	ctx context.Context, req *messagerserver.JoinChatRequest,
) (*messagerserver.JoinChatResponse, error) {
	if err := s.Chat.JoinChat(ctx, req.Token, req.ChatID, req.UserID); err != nil {
		return &messagerserver.JoinChatResponse{JoinChatStatus: messagerserver.JoinChatStatus_JoinUnsuccessful}, nil
	}

	return &messagerserver.JoinChatResponse{JoinChatStatus: messagerserver.JoinChatStatus_JoinSuccessful}, nil
}

func (s *GRPCService) LeaveChat(
	ctx context.Context, req *messagerserver.LeaveChatRequest,
) (*messagerserver.LeaveChatResponse, error) {
	if err := s.Chat.LeaveChat(ctx, req.Token, req.ChatID, req.UserID); err != nil {
		return &messagerserver.LeaveChatResponse{LeaveChatStatus: messagerserver.LeaveChatStatus_LeaveUnsuccessful}, nil
	}

	return &messagerserver.LeaveChatResponse{LeaveChatStatus: messagerserver.LeaveChatStatus_LeaveSuccessful}, nil
}
