package service

import (
	"context"
	"errors"
	"gomessanger/usecases"
)

var (
	ErrUserAlreadyExists = errors.New("user already exists")
	ErrLoginOrPassWrong  = errors.New("login or password is wrong")
	ErrUnknown           = errors.New("unknown error")
)

type User struct {
	UserUseCases usecases.User
}

func (user *User) Register(ctx context.Context, login, pass string) error {
	err := user.UserUseCases.Register(ctx, login, pass)
	switch {
	case errors.Is(err, usecases.ErrUserAlreadyExists):
		return ErrUserAlreadyExists
	case err != nil:
		return ErrUnknown
	default:
		return nil
	}
}

func (user *User) Login(ctx context.Context, login, pass string) (string, error) {
	loginToken, err := user.UserUseCases.Login(ctx, login, pass)

	switch {
	case errors.Is(err, usecases.ErrLoginOrPassWrong):
		return "", ErrLoginOrPassWrong
	case err != nil:
		return "", ErrUnknown
	default:
		return loginToken, nil
	}
}
