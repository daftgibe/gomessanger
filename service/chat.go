package service

import (
	"context"
	"fmt"
	"gomessanger/usecases"
)

type Chat struct {
	ChatUseCases usecases.Chat
}

func (ch *Chat) CreateChat(ctx context.Context, token string, chatID, userID uint64) error {
	if err := ch.ChatUseCases.CreateChat(ctx, token, chatID, userID); err != nil {
		return fmt.Errorf("cannot create chat | %w", err)
	}

	return nil
}

func (ch *Chat) DeleteChat(ctx context.Context, token string, chatID uint64) error {
	if err := ch.ChatUseCases.DeleteChat(ctx, token, chatID); err != nil {
		return fmt.Errorf("cannot delete chat | %w", err)
	}

	return nil
}

func (ch *Chat) JoinChat(ctx context.Context, token string, chatID, userID uint64) error {
	if err := ch.ChatUseCases.JoinChat(ctx, token, chatID, userID); err != nil {
		return fmt.Errorf("cannot join chat | %w", err)
	}

	return nil
}

func (ch *Chat) LeaveChat(ctx context.Context, token string, chatID, userID uint64) error {
	if err := ch.ChatUseCases.LeaveChat(ctx, token, chatID, userID); err != nil {
		return fmt.Errorf("cannot leave chat | %w", err)
	}

	return nil
}
