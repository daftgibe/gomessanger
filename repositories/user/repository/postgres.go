package repository

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Postgres struct {
	pool *pgxpool.Pool
}

func NewPostgres(pool *pgxpool.Pool) *Postgres {
	return &Postgres{pool: pool}
}

func (p *Postgres) Exist(ctx context.Context, login string) (bool, error) {
	var exist bool

	err := p.pool.QueryRow(ctx, "select exists(select 1 from messenger.users where login=$1)", login).Scan(&exist)

	switch {
	case err == pgx.ErrNoRows:
		return false, nil
	case err != nil:
		return false, fmt.Errorf("exists failed | %w", err)
	default:
		return exist, nil
	}
}

func (p *Postgres) Insert(ctx context.Context, login, pass string) error {
	if _, err := p.pool.Exec(ctx, "insert into messenger.users(login,pass) values ($1,$2)", login, pass); err != nil {
		return fmt.Errorf("insert failed | %w", err)
	}

	return nil
}

func (p *Postgres) Login(ctx context.Context, login, pass string) (bool, error) {
	var exist bool

	err := p.pool.QueryRow(
		ctx, "select exists(select 1 from messenger.users where login=$1 and pass=$2)", login, pass,
	).Scan(&exist)

	switch {
	case err == pgx.ErrNoRows:
		return false, nil
	case err != nil:
		return false, fmt.Errorf("login failed | %w", err)
	default:
		return exist, nil
	}
}
