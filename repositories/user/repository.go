package user

import "context"

//go:generate mockgen -source=repository.go -destination=repository/mock.go -package=repository
type Repository interface {
	Exist(ctx context.Context, login string) (bool, error)
	Insert(ctx context.Context, login, pass string) error
	Login(ctx context.Context, login, pass string) (bool, error)
}
