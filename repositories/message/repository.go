package message

import (
	"context"
	"time"
)

type UnreadMessage struct {
	ChatID, UserID uint64
	MessageText    string
	DateSent       time.Time
}

type Repository interface {
	SetMessage(ctx context.Context, chatID, userID uint64, messageText, dateSent string) error
	GetUnreadMessages(ctx context.Context, chatID, userID uint64) ([]UnreadMessage, error)
	DeleteMessages(ctx context.Context, chatID uint64) error
}
