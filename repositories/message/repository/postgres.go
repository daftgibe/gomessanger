package repository

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gomessanger/repositories/message"
)

var ErrNoNewMessages = errors.New("no new messages")

type Postgres struct {
	pool *pgxpool.Pool
}

func NewPostgres(pool *pgxpool.Pool) *Postgres {
	return &Postgres{pool: pool}
}

func (p *Postgres) SetMessage(ctx context.Context, chatID, userID uint64, messageText, dateSent string) error {
	_, err := p.pool.Exec(
		ctx, "insert into messenger.chats values($1,$2,$3,$4)", chatID, userID, messageText, dateSent,
	)
	if err != nil {
		return fmt.Errorf("cannot set message | %w", err)
	}

	return nil
}

func (p *Postgres) GetUnreadMessages(
	ctx context.Context, chatID, userID uint64,
) ([]message.UnreadMessage, error) {
	rows, err := p.pool.Query(
		ctx,
		"select * from messenger.chats "+
			"where chat_id=$1 and date_sent > (select last_read_time from messenger.users_chats where chat_id=$2 and user_id=$3)",
		chatID, chatID, userID,
	)
	if err != nil {
		return nil, fmt.Errorf("cannot get undelivered messages | %w", err)
	}

	defer rows.Close()

	nextRow := rows.Next()
	if !nextRow {
		return nil, fmt.Errorf("cannot get undelivered messages | %w", ErrNoNewMessages)
	}

	var unreadMessages = make([]message.UnreadMessage, 0)

	for ; nextRow; nextRow = rows.Next() {
		unreadMessage := message.UnreadMessage{}

		if err = rows.Scan(&unreadMessage.ChatID, &unreadMessage.UserID, &unreadMessage.MessageText, &unreadMessage.DateSent); err != nil {
			return nil, fmt.Errorf("cannot get undelivered messages | %w", err)
		}

		unreadMessages = append(unreadMessages, unreadMessage)
	}

	return unreadMessages, nil
}

func (p *Postgres) DeleteMessages(ctx context.Context, chatID uint64) error {
	_, err := p.pool.Exec(ctx, "delete from messenger.chats where chat_id=$1", chatID)
	if err != nil {
		return fmt.Errorf("cannot delete chat | %w", err)
	}

	return nil
}
