package chat

import "context"

type Repository interface {
	IsChatExist(ctx context.Context, chatID uint64) (bool, error)
	IsUserInChat(ctx context.Context, chatID, userID uint64) (bool, error)
	SetUserInChat(ctx context.Context, chatID, userID uint64) error
	DropUserFromChat(ctx context.Context, chatID, userID uint64) error
	DeleteChat(ctx context.Context, chatID uint64) error
	SetLastChatReadingTime(ctx context.Context, chatID, userID uint64) error
}
