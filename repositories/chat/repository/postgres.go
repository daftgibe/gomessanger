package repository

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"time"
)

type Postgres struct {
	pool *pgxpool.Pool
}

type Storage struct {
	*pgxpool.Pool
}

func NewPostgres(pool *pgxpool.Pool) *Postgres {
	return &Postgres{pool: pool}
}

func (p *Postgres) IsChatExist(ctx context.Context, chatID uint64) (bool, error) {
	var exist bool

	err := p.pool.QueryRow(ctx, "select exists(select 1 from messenger.users_chats where chat_id=$1)", chatID).Scan(&exist)
	switch {
	case err == pgx.ErrNoRows:
		return false, nil
	case err != nil:
		return false, fmt.Errorf("exists failed | %w", err)
	default:
		return exist, nil
	}
}

func (p *Postgres) IsUserInChat(ctx context.Context, chatID, userID uint64) (bool, error) {
	var exist bool
	err := p.pool.QueryRow(ctx, "select exists(select 1 from messenger.users_chats where chat_id=$1 and user_id=$2)", chatID, userID).Scan(&exist)

	switch {
	case err == pgx.ErrNoRows:
		return false, nil
	case err != nil:
		return false, fmt.Errorf("exists failed | %w", err)
	default:
		return exist, nil
	}
}

func (p *Postgres) SetUserInChat(ctx context.Context, chatID, userID uint64) error {
	_, err := p.pool.Exec(ctx, "insert into messenger.users_chats values ($1,$2,$3)", chatID, userID, time.Now())
	if err != nil {
		return fmt.Errorf("cannot set user in chat | %w", err)
	}

	return nil
}

func (p *Postgres) DropUserFromChat(ctx context.Context, chatID, userID uint64) error {
	_, err := p.pool.Exec(ctx, "delete from messenger.users_chats where chat_id=$1 and user_id=$2", chatID, userID)
	if err != nil {
		return fmt.Errorf("cannot drop user from chat | %w", err)
	}

	return nil
}

func (p *Postgres) DeleteChat(ctx context.Context, chatID uint64) error {
	_, err := p.pool.Exec(ctx, "delete from messenger.users_chats where chat_id=$1", chatID)
	if err != nil {
		return fmt.Errorf("cannot delete chat | %w", err)
	}

	return nil
}

func (p *Postgres) SetLastChatReadingTime(ctx context.Context, chatID, userID uint64) error {
	_, err := p.pool.Exec(ctx, "update messenger.users_chats set last_read_time=$1 where chat_id=$2 and user_id=$3", time.Now(), chatID, userID)
	if err != nil {
		return fmt.Errorf("cannot set last chat reading time | %w", err)
	}

	return nil
}
